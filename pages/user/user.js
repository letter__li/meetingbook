Page({
    data: {
        username: "masterXiao",
        sex: "男",
        email: "example@163.com",
        contact: "138123456789",
        "address": "冰岛"
    },
    logout: function () {
        wx.showModal({
            title: "提示",
            content: "是否退出当前账号",
            success: function (res) {
                if (res.confirm) {
                    wx.clearStorageSync();
                    wx.redirectTo({
                        url: "../login/login",
                    }) 
                } else if (res.cancel) {
                    console.log("用户取消按钮");
                }
            }
        })
    }
})