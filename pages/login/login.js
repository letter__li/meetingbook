Page({
    data: {
        account: "masterXiao",
        password: "123456"
    },
    // 获取输入账号
    accountInput: function (e) {
        this.setData({
            account: e.detail.value
        });
    },
    // 获取输入密码
    passwordInput: function (e) {
        this.setData({
            password: e.detail.value
        });
    },
    // 登录
    login: function () {
        if (this.data.account.length == 0) {
            wx.showToast({
                title: "账号不能为空！",
                image: "../../images/failmsg.png",
                duration: 1000
            })
            return;
        }
        if (this.data.password.length == 0) {
            wx.showToast({
                title: "密码不能为空！",
                image: "../../images/failmsg.png",
                duration: 1000
            })
            return;
        }
        if (this.data.account != "masterXiao" && this.data.password != "123456") {
            wx.showToast({
                title: "账号密码错误！",
                image: "../../images/failmsg.png",
                duration: 1000
            })
            return;
        }
        wx.switchTab({
            url: "../index/index",
        })
    },
    // QQ登录
    qqLogin: function () {
        console.log("QQ授权登录");
    },
    // Sina登录
    sinaLogin: function () {
        console.log("Sina授权登录");
    },
    // Wechat登录
    wechatLogin: function () {
        console.log("Wechat授权登录");
    }
})

